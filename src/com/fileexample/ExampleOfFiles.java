package com.fileexample;

import java.io.FileOutputStream;
import java.util.Scanner;

public class ExampleOfFiles {
    String line;
    String copyLine;
    byte[] copyArray;

    void createTextFile() {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("D:\\Tables\\File\\FilesExample.txt");

            Scanner scanner = new Scanner(System.in);
            System.out.println("Write a lovely line to store");
             line = scanner.nextLine();
            byte[] stringArray = line.getBytes();
            fileOutputStream.write(stringArray);
            fileOutputStream.close();
            System.out.println("File is written successfully");

        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
    //writing file
    void copyFileByteByByte(){
        try{
            FileOutputStream fileOutputStreamObj = new FileOutputStream("D:\\Tables\\File\\CopyFileExample.txt");
            copyLine = line;

             copyArray= copyLine.getBytes();
            fileOutputStreamObj.write(copyArray);
            fileOutputStreamObj.close();
            System.out.println("File copy byte by byte is done");

        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
    void copyFileOnce(){
        try{
            FileOutputStream fileOutputStreamCopy = new FileOutputStream("D:\\Tables\\File\\CopyFileOnce.txt");
            fileOutputStreamCopy.write(copyArray);
            fileOutputStreamCopy.close();
            System.out.println("File is Copied Successfully at a time");

        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }

    public static void main(String[] args) {
        ExampleOfFiles exampleOfFilesObj = new ExampleOfFiles();
        exampleOfFilesObj.createTextFile();
        exampleOfFilesObj.copyFileByteByByte();
        exampleOfFilesObj.copyFileOnce();

    }

}
