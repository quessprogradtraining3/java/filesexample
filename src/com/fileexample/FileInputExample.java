package com.fileexample;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

public class FileInputExample {
    void display(){
        int i= 0;
        try{
            FileInputStream fileInputStreamObject = new FileInputStream("D:\\Tables\\File\\example.txt");

            while( (i=fileInputStreamObject.read())!=-1){
                System.out.print((char)i);
            }

        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
    //fileinputstream is for reading the file
    void write(){
        try{
            FileOutputStream fileOutputStreamObj = new FileOutputStream("D:\\Tables\\File\\example.txt");
           // fileOutputStreamObj.write(65);
            Scanner scanner = new Scanner(System.in);
            String input = scanner.next();
            byte[] stringArray = input.getBytes();
            fileOutputStreamObj.write(stringArray);
            fileOutputStreamObj.close();
            System.out.println("File written succesfully");
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
    //fileoutputstream for writing the file

    public static void main(String[] args) {
        FileInputExample fileInputExample = new FileInputExample();
        fileInputExample.display();
        fileInputExample.write();
    }
}
